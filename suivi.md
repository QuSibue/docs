# Suivi de projet
## Projet STAPS : TROP'S

### Au préalable
Contact avec le groupe de STAPS pour poser les bases du projet.  
Réalisation d'une première maquette et différentes ébauches d'interfaces. En voici un exemple :  
<img src="/assets/maquette.png" alt="Maquette" height="250">  
   
Discussions sur le cadre à donner au projet.

------------------------------------

### SEMAINE 0 : du 27/01 au 31/01
#### Mardi 28/01
Présentation des projets.  
Installation d'Android Studio et des autres logiciels de développement ([Postman](https://www.postman.com), [Visual Studio Code](https://code.visualstudio.com/)).  
Prise en main du framework [Flutter](https://flutter.dev).  
Réalisation d'une première application [test](https://flutter.dev/docs/development/ui/widgets-intro). Cette application a pour but de nous familiariser avec les différents layouts et widgets de flutter. 

#### Mercredi 29/01
Réunion avec l'équipe des STAPS.  
Choix de la charte graphique de l'application (couleurs, thème).  
Ebauche des écrans de l'application.  
Création d'user stories pour guider le premier sprint. Elles sont disponibles dans le [tableau de bord](https://gitlab.com/projet_info5/trops_app/-/boards) du projet.

#### Vendredi 31/01
Début de développement du frontend.  
Distribution des différentes pages de l'application :
- Recherche annonce -> Ariane
- Home -> Mathieu
- Profile -> Tanguy
- Création annonce -> Quentin 

Utilité des différentes pages :
- Home : page par défaut de l'application, les annonces y sont affichés ainsi qu'une barre de recherche. Nous pouvons accéder aux autres pages depuis celle-ci.
- Recherche annonce : permet de rechercher une annonce grâce à son nom.
- Profile : affiche des informations concernant l'utilisateur.
- Création annonce : permet de créer une annonce pour que celle-ci s'affiche sur la page principale.

------------------------------------

### SEMAINE 1 : du 03/02 au 07/02

#### Lundi 03/02
Réunion d'avancement avec Didier Donsez.  
Développement du frontend :
- Page Home :
    + Création d'un Widget advertTile qui permet l'affichage d'une annonce grâce à un objet Advert.
    + Gestion des overflows lors de l'affichage des annonces.
- Page Recherche annonce : 
    + Récupération des annonces depuis l'objet Advert.
    + Récupération du texte tapé dans la barre de recherche.
- Page Création annonce : 
    + Création de différents champs pour renseigner les informations.
    + Ajout d'un bouton de validation qui vérifie que les champs soient remplis.
- La page Profile est en stand-by, le temps de créer une page de login.
- Création d'une page de login basée sur le projet [TheGorgeousLogin](https://github.com/huextrat/TheGorgeousLogin/).

Paramétrage d'un backend d'authentification grâce à [NodeJs](https://nodejs.org/fr/), [Express](https://expressjs.com/) et [Mongoose](https://mongoosejs.com/).   
L'objet Advert permet de définir tous les champs d'une annonce : titre, prix, description, ...

#### Mardi 04/02
Développement du frontend : 
- Page Home :
    + Ajout d'une barre de navigation en bas de l'application qui permet de naviguer entre les différentes pages.
    + Affichage d'une image pour chacune des annonces.
- Page Recherche annonce : 
    + Création d'un panneau latéral qui permet de limiter le prix d'une annonce recherchée.
- Page Création annonce : 
    + Ajout d'un champ photo qui permet de choisir/prendre 4 photos et de les ajouter à l'annonce.
- Page login : 
    + Liaison avec le backend pour la connection de l'utilisateur.

Fin de paramétrage du backend.

#### Mercredi 05/02
Développement du frontend : 
- Page Home :
    + Refonte de l'interface : simplification des composants/optimisation (on réduit le nombre de lignes de codes pour le même affichage).
- Page création d'annonce : 
    + Refonte de l'interface pour coller avec le reste de l'application.
- Correction de bugs concernant le comportement de certains composants des pages.

Développement du backend : 
- Début de développement d'une API permettant la gestion des annonces faites sur l'application.
- Fin de l'intégration de l'API de gestion d'authentifaction à l'application.  

#### Jeudi 06/02
Réunion hebdomadaire avec les étudiants en STAPS.
- Système de profil (infos, mes annonces, mes favoris).
- Systeme de messageries pour échanges entre les 2 parties.
- Géolocaliser les annonces.
- Système de badges pour les utilisateurs ?
- Enregistrer les annonces (favoris).


#### Vendredi 07/02
Développement du frontend : 
- Page Profile :
    + Affichage du nom / mail de l'utilisateur.
    + Ajout d'un bouton de déconnection.
- Page Home :
    + Affichage des catégories en bas à gauche des annonces.
- Page détails annonces :
    + Création de la page
    + Détails de l'annonce -> titre, prix, description...
Développement du backend : 
- Ajout d'endpoints sur l'API afin de créer / supprimer une annonce.

------------------------------------

### SEMAINE 2 : du 10/02 au 14/02

#### Lundi 10/02
Développement du frontend : 
- Page création d'annonce :
    + Ajout du choix de la catégorie.
- Page Home :
    + Affichage des catégories en haut de la page.
    + On peut cliquer sur les cétagories pour lancer une recherche.
- Page de recherche :
    + Ajout du filtre par catégorie.

Développement du backend : 
- Création d'une documentation swagger pour l'API.

#### Mardi 11/02
Développement du frontend : 
- Page profile :
    + Ajout d'une liste d'annonces favorites.
- Page de détails :
    + Ajout d'un bouton pour ajouter l'annonce aux favoris.
- Page de création d'annonce :
    + Ajout d'un champ disponibilités.

#### Mercredi 12/02  
Développement du frontend : 
- Page de détails :
    + Affichage des dates de disponibilités.

Développement du backend : 
- Ajout des endpoints pour les annonces et les catégories. 

#### Jeudi 13/02
Réunion hebdomadaire avec les étudiants en STAPS.
- Image en arrière plan dans la liste des catégories.
- Logo lors du chargement + en haut de l’appli.
- Création d’annonce par étape.

#### Vendredi 14/02
Développement du frontend : 
- Page de création d'annonce :
    + Modification de l'interface pour faire des étapes lors de la création.
- Ajout du logo de chargement au lancement de l'app.
- Page Home :
    + Ajout des images d'arrière plan pour les catégories.

Développement du backend : 
- Modification de la gestion des catégories pour pouvoir créer des sous-catégories. (ex : Sports d'hiver -> Ski).

------------------------------------

### SEMAINE 3 : du 17/02 au 21/02

#### Mardi 18/02
Mise en place d'un système d'upload de photo sur le serveur :
- Lorsque l'utilisateur décide d'ajouter ue photo à son annonce, celle-ci est compressée puis envoyée sur le serveur.
- Pour ne pas avoir de doublon nous avons décidé de modifier le nom des fichiers photos en effectuant un hash de l'ancien + des données de temps.

#### Mercredi 19/02
Développement du frontend : 
- Sélection des disponibilités pour chaque annonce.
- Recherche avancée : plus de filtres.

Développement du backend : 
- Ajout des endpoints pour la recherche avancée.

#### Jeudi 20/02
Réunion hebdomadaire avec les étudiants en STAPS.
- Possibilité de modifier les annonces.
- Laisser emplacement pour les pubs.

------------------------------------

### SEMAINE 4 : du 24/02 au 28/02, INTERRUPTION PEDAGOGIQUE
------------------------------------

### SEMAINE 5 : du 02/03 au 06/03
Pour avancer plus rapidement sur certains aspects, division du travail en 4, un sprint par personne sur la semaine.

#### SPRINT 1 : WEBADMIN (Tanguy)
Objectif : Réaliser l'interface d'administration pour l'application
Contenu : 
- Contrôle des utilisateurs
- Contrôles des annonces
- Contrôles des catégories
- Visualisation de l'état de l'API et du serveur (Stockage restant, nombre d'objets stockés dans la BD, ...)

Utilisation de [JHipster](https://jhipster.tech) en mode front-end only avec [React](https://reactjs.org/)

#### SPRINT 2 : LOCALISATION (Mathieu)
Objectif: Localiser les annonces
Contenu : 
- Attribuer un champ localisation à la structure Advert de la base de donnée
- Utiliser l' [API de géolocalisation du gouvernement](https://geo.api.gouv.fr/)
- Localiser l'utilisateur lors de la création d'une annonce
- Localiser l'utilisateur pour affiner les résultat de recherche

#### SPRINT 3 : IMAGE (Quentin)
Objectif : Refonte du système d'image
Contenu:
- Le serveur renvoit le lien de l'image uploadée
- Renommer les images avec un Hash
- Créer une classe encapsulant les méthodes liées à l'upload d'images


#### SPRINT 4 :  MODIFICATION DES ANNONCES (Ariane)
Objectif : Permettre la modification d'une annonce par son propriétaire
Contenu :
 - Changement du titre, description, champs textes, ...
 - Ajout/Suppression de photos
 - Changement de catégorie

------------------------------------

### SEMAINE 6 : du 09/03 au 13/03
Objectifs de la semaine, suivant l'avancée de la semaine /sprint/
- BACKEND
    + Nettoyage de l'API et des endpoints inutilisés
    + Mise à jour de la doc Swagger
- USER FRONTEND 
    + Système de réservation basique
    + Refonte de l'interface de création d'annonces
- ADMIN FRONTEND
    + Visualisation de l'état du serveur

------------------------------------

### SEMAINE 7 : du 16/03 au 20/03
#### Mardi 17/03
Réunion avec Didier Donsez
Rédaction de documentation:
- Rapport MPI
- Documentation technique
- README des repository


#### Mercredi 18/03, Jeudi 19/03 & Vendredi 20/03
Rédaction de documentation :
- Rapport projet
- Guide d'installation de Flutter

