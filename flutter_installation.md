# How to install Flutter via Android Studio

This document is intended to explain how to install Flutter via Android Studio. As the Android Studio interface is updated periodically, this document may become obsolete with versions older than March 2020.


## PRELIMINARIES
Flutter being a Google technology, it is possible to install it via Android Studio, the IDE dedicated to mobile development for the Google OS. You will therefore need the latter, which can be downloaded at the following address : [Download Android Studio](https://developer.android.com/studio)  
Note that it is not necessary to use Android Studio to develop in Flutter, VSCode & IntelliJ are also compatible but are not concerned by this document.

## ANDROID STUDIO SETUP
1. Open Android Studio, click on ***Configure -> Plugins***
<img src="assets/etape1.png"  width="300" height="349">

2. In the searchbar type "flutter" and click on the first result
<img src="assets/etape2.png"  width="527" height="441">

3. Once in the details of the plugins click on "install"
<img src="assets/etape3.png"  width="528" height="443">

4. If you don't have installed Dart yet, a popup will warn you to download it, click "Yes"
<img src="assets/etape4.png"  width="251" height="38">

5. Wait for the download to be completed
<img src="assets/etape5.png"  width="235" height="45">

6. Once the download is done, just restart Android Studio and see the new option to start a project
<img src="assets/etape6.png"  width="271" height="200">

