# TROPS - Technical document

This is a rather exhaustive document about the technical parts of the TROPS Projet. To get an overview of the project, please refer to the project report.


##### Contact
If you have any question, you can contact us using any of these emails
- Tanguy [tanguy@sauton.xyz]()
- Mathieu [mathuld123456@hotmail.fr]() 
- Quentin [quentin.sibue@live.fr]()
- Ariane [ancrenaz.ariane@hotmail.fr]()



## FRONTEND 
#### [FLUTTER APP](https://gitlab.com/projet_info5/trops_app)
Our application is developped in Flutter. Flutter is a Google framework meant to create cross-platform (that is to say iOS+Android) applications. You can learn more about flutter using the following ressources :
- [flutterawesome.com](https://flutterawesome.com/) : Designs and code samples
- [flutter.dev](https://flutterawesome.com/) : Official flutter documentation made by google
- [Flutter Catalog](https://play.google.com/store/apps/details?id=io.github.x_wei.flutter_catalog) : Helper application available on Google Play Store.
- [Flutter Installation Guide](/flutter_installation.md) : A little guide to install Flutter with Android Studio.

#### [WEB ADMIN PANEL](https://gitlab.com/projet_info5/trops_admin-panel)

You will find a detailed documentation in the repository README


## BACKEND
#### [API](https://gitlab.com/projet_info5/trops_api)

You will find a detailed documentation in the repository README


##### AUTHENTICATION

###### Standard
Email + Password authentication is done using [JWT - Json Web Token](https://jwt.io/)  
Password are stored in the database as hash.

###### Google
TROPS supports Google Authentication via the flutter package [google_sign_in](https://pub.dev/packages/google_sign_in)  
The Google API delivers 2 keys:
-An OAuth2.0 API Token for accessing Google Ressources (such as Google Maps) and
-An [OpenID Token](https://developers.google.com/identity/protocols/oauth2/openid-connect) which is a JWT that contains informations about the user that we will use for register/login.

A specific endpoint of the api is used for social login [https://api.trops.space/auth/google/]()

A dedicated Google Account was created for managing the services (admin@trops.space). To get access to the TROPS Google Account, feel free to contact us.


#### [DATABASE (MongoDB)](https://www.mongodb.com/)

Our database choice for this project is _MongoDB_.
The reason for this choice are the huge adoption of Mongo in multiple platforms, the huge quantity of library available for multiple languages and its simplicity to use.  
Our development and early-production database is host on [MongoDB Atlas](https://www.mongodb.com/cloud/atlas) cloud platform.  

## SERVER

##### DEVELOPMENT ENVIRONMENT
With our limited ressources, we managed to deploy our all stack on a single virtual server. 

If you want to know more about how to deploy multiple web services using a single server, feel free to contact us

## DOMAIN NAME
TROPS development domain name is [trops.space](). For domain name transfer or related questions, feel free to contact us
